//#include <Arduino.h>
extern "C"
{
#include "user_interface.h" // Required for wifi_station_connect() to work
}
#include <ESP8266WiFi.h> // https://github.com/esp8266/Arduino
#include <EEPROM.h>
#include <FS.h>
#include <ESPAsyncTCP.h>
#include <ESPAsyncWebServer.h>

#include <ESPAsyncWiFiManager.h> // https://github.com/tzapu/WiFiManager
#include <errno.h>
#include <Adafruit_NeoPixel.h>
// Led Config
int PinArray[6] = {D1, D2, D3, D4, D5, D6}; // Digital IO pin connected to the Led.

#define PIXEL_COUNT 68 // Count of Led

#define EEPROM_SIZE 128 // size of NV-memory

// structure holds the parameters to save nonvolatile
typedef struct
{
  unsigned char mode;
  unsigned char r, g, b;
  unsigned char brightness;
  unsigned char valid;
} wuerfel_processor_struct;
wuerfel_processor_struct wuerfel_processor;
wuerfel_processor_struct *wuerfel = &wuerfel_processor;

unsigned char *eptr;

// WiFi-objects
WiFiUDP ntpUDP;
// the web-server-object
AsyncWebServer server(80);
DNSServer dns;

AsyncWiFiManager wifiManager(&server, &dns);

/**
 * Control the Leds 
 */
class Led : public Adafruit_NeoPixel
{
public:
  Led();
  void setup(int pins[], int pinCount, int pixelCount);
  void Start();
  void SelectShow(int mode);
  void setPixelColors(int Red, int Green, int Blue);

private:
  void ShowColor(int pixelcnt, int Red, int Green, int Blue);
  void StartStandardShow();
  void StartRainbow();
  void AllShows();
  void Rainbow(int red, int green, int blue);
  void MoveBottomToTop();
  void AnimationHelper();
  int *pins;
  int **PixelOnOff;
  int pinCount;
  int pixelCount;
  int Red = 0;
  int Green = 0;
  int Blue = 0;
};
/**
 * Led Constructor
 * @param int Led Digital Pin
 */
Led::Led()
{
  //   NEO_RGB     Pixels are wired for RGB bitstream
  //   NEO_GRB     Pixels are wired for GRB bitstream, correct for neopixel stick
  //   NEO_KHZ400  400 KHz bitstream (e.g. FLORA pixels)
  //   NEO_KHZ800  800 KHz bitstream (e.g. High Density LED strip), correct for neopixel stick
Adafruit_NeoPixel:
  Adafruit_NeoPixel();
  this->updateType(NEO_GRB + NEO_KHZ800);
  this->updateLength(PIXEL_COUNT);
}
/**
 * Setup Led Class
 * @param pins Digital pins
 * @param pinCount Number of Pins
 * @param PixelCount Number of Pixels per pin
 */
void Led::setup(int pins[], int pinCount, int pixelCount)
{
  this->pins = pins;
  this->pinCount = pinCount;
  this->pixelCount = pixelCount;
  this->PixelOnOff = new int *[this->pinCount];
  for (int i = 0; i < this->pinCount; i++)
  {
    this->PixelOnOff[i] = new int[pixelCount];
  }
}

void Led::setPixelColors(int Red, int Green, int Blue)
{
  this->Red = Red;
  this->Green = Green;
  this->Blue = Blue;
}

/**
 * Start with LED
 */
void Led::Start()
{
  for (int i = 0; i < this->pinCount; i++)
  {
    this->setPin(this->pins[i]);

    this->begin();
    this->clear();
    this->show();
  }
}
/**
 * Show Handler cals next Show an Stop current
 */
void Led::SelectShow(int mode)
{
  if (mode == 2)
  {
    this->StartRainbow();
  }
  else if (mode == 3)
  {
    this->MoveBottomToTop();
  }
  else if (mode == 4)
  {
    this->AllShows();
  }
  else
  {
    this->StartStandardShow();
  }
}
/**
 * Shows combined
 */
void Led::AllShows()
{

  Serial.println("All");
  this->setPixelColors(230, 92, 12);
  this->StartStandardShow();
  delay(5000);
  this->MoveBottomToTop();
  this->MoveBottomToTop();
  this->MoveBottomToTop();
  this->setPixelColors(255, 0, 0);
  this->StartStandardShow();
  delay(4000);
  this->StartRainbow();
  this->StartRainbow();
  this->setPixelColors(46, 59, 230);
  this->StartStandardShow();
  delay(5000);
  this->MoveBottomToTop();
  this->MoveBottomToTop();
  this->setPixelColors(63, 127, 93);
  delay(3000);
  this->MoveBottomToTop();
}
/**
 * Animation from bottom to top and revert
 */
void Led::MoveBottomToTop()
{
  Serial.println("Bottomtotop");
  for (int i = 0; i < this->pinCount; i++)
  {
    this->setPin(this->pins[i]);
    this->begin();
    this->clear();
    this->show();
    for (int k = 0; k <= PIXEL_COUNT; k++)
    {
      this->PixelOnOff[i][k] = 0;
    }
  }

  int pin0Countup = 34;
  int pin0Countdown = 33;
  int pin1Countup = 0;
  int pin1Countdown = 66;
  int pin2Countup = 0;
  int pin2Countdown = 66;
  int delaytime = 400;
  // first
  for (int i = 0; i <= 16; i++)
  {
    this->PixelOnOff[0][pin0Countdown] = 1; // backward
    this->PixelOnOff[0][pin0Countup] = 1;   // forward
    pin0Countup++;
    pin0Countdown--;

    this->PixelOnOff[1][pin1Countup] = 1;   // forward
    this->PixelOnOff[1][pin1Countdown] = 1; //backward
    pin1Countup++;
    pin1Countdown--;

    this->PixelOnOff[2][pin2Countup] = 1;   // forward
    this->PixelOnOff[2][pin2Countdown] = 1; //backward
    pin2Countup++;
    pin2Countdown--;

    this->AnimationHelper();
    delay(delaytime);
    delaytime -= 10;
  }

  int pin4Countup = 51;
  int pin4Countdown = 50;
  int pin5Countup = 17;
  int pin5Countdown = 16;
  int pin3Countup = 50;
  int pin3Countdown = 49;
  // middle
  for (int i = 0; i <= 16; i++)
  {

    this->PixelOnOff[0][pin0Countdown] = 1; // backward
    this->PixelOnOff[0][pin0Countup] = 1;   // forward
    pin0Countup++;
    pin0Countdown--;

    this->PixelOnOff[1][pin1Countup] = 1;   // forward
    this->PixelOnOff[1][pin1Countdown] = 1; //backward
    pin1Countup++;
    pin1Countdown--;

    this->PixelOnOff[2][pin2Countup] = 1;   // forward
    this->PixelOnOff[2][pin2Countdown] = 1; //backward
    pin2Countup++;
    pin2Countdown--;

    this->PixelOnOff[3][pin3Countup] = 1;   // forward
    this->PixelOnOff[3][pin3Countdown] = 1; //backward
    pin3Countup++;
    pin3Countdown--;
    this->PixelOnOff[4][pin4Countdown] = 1; // backward
    this->PixelOnOff[4][pin4Countup] = 1;   // forward
    pin4Countup++;
    pin4Countdown--;

    this->PixelOnOff[5][pin5Countdown] = 1; // backward
    this->PixelOnOff[5][pin5Countup] = 1;   // forward
    pin5Countup++;
    pin5Countdown--;

    this->AnimationHelper();
    delay(delaytime);
    if (i <= 8)
    {
      delaytime -= 10;
    }
    else
    {
      delaytime += 10;
    }
  }
  // end

  pin3Countup = 0;
  pin4Countup = 0;
  pin5Countdown = 66;
  pin5Countup = 33;
  pin3Countdown = 33;

  for (int i = 0; i <= 16; i++)
  {

    this->PixelOnOff[3][pin3Countup] = 1;   // forward
    this->PixelOnOff[3][pin3Countdown] = 1; //backward
    pin3Countup++;
    pin3Countdown--;
    this->PixelOnOff[4][pin4Countdown] = 1; // backward
    this->PixelOnOff[4][pin4Countup] = 1;   // forward
    pin4Countup++;
    pin4Countdown--;

    this->PixelOnOff[5][pin5Countdown] = 1; // backward
    this->PixelOnOff[5][pin5Countup] = 1;   // forward
    pin5Countup++;
    pin5Countdown--;

    this->AnimationHelper();
    delay(delaytime);
    delaytime += 10;
  }
  // backward
  // pixel count correct
  pin3Countdown++;
  pin5Countup--;
  //pin4Countdown++;
  delay(1000);
  // back first

  pin5Countup = 50;
  pin3Countdown = 16;

  for (int i = 0; i <= 17; i++)
  {
    this->PixelOnOff[3][pin3Countup] = 0;   // forward
    this->PixelOnOff[3][pin3Countdown] = 0; //backward
    pin3Countup--;
    pin3Countdown++;
    this->PixelOnOff[4][pin4Countdown] = 0; // backward
    this->PixelOnOff[4][pin4Countup] = 0;   // forward
    pin4Countup--;
    pin4Countdown++;

    this->PixelOnOff[5][pin5Countdown] = 0; // backward
    this->PixelOnOff[5][pin5Countup] = 0;   // forward
    pin5Countup--;
    pin5Countdown++;

    this->AnimationHelper();
    delay(delaytime);
    delaytime -= 10;
  }

  pin0Countdown = 0; //-1  oben
  pin0Countup = 67;
  pin1Countdown = 33;
  pin1Countup = 33;
  pin2Countdown = 33;
  pin2Countup = 33;
  pin3Countup = 66;   // unten --
  pin3Countdown = 33; // unten
  pin4Countup = 67;   // hinten
  pin5Countdown = 0;  //-1
  pin5Countup = 33;

  // back middle
  for (int i = 0; i <= 17; i++)
  {
    this->PixelOnOff[0][pin0Countdown] = 0; // backward
    this->PixelOnOff[0][pin0Countup] = 0;   // forward
    pin0Countup--;
    pin0Countdown++;

    this->PixelOnOff[1][pin1Countup] = 0;   // forward
    this->PixelOnOff[1][pin1Countdown] = 0; //backward
    pin1Countup--;
    pin1Countdown++;

    this->PixelOnOff[2][pin2Countup] = 0;   // forward
    this->PixelOnOff[2][pin2Countdown] = 0; //backward
    pin2Countup--;
    pin2Countdown++;

    this->PixelOnOff[3][pin3Countup] = 0;   // forward
    this->PixelOnOff[3][pin3Countdown] = 0; //backward
    pin3Countup--;
    pin3Countdown++;
    this->PixelOnOff[4][pin4Countdown] = 0; // backward
    this->PixelOnOff[4][pin4Countup] = 0;   // forward
    pin4Countup--;
    pin4Countdown++;

    this->PixelOnOff[5][pin5Countdown] = 0; // backward
    this->PixelOnOff[5][pin5Countup] = 0;   // forward
    pin5Countup--;
    pin5Countdown++;

    this->AnimationHelper();
    delay(delaytime);
    if (i <= 8)
    {
      delaytime -= 10;
    }
    else
    {
      delaytime += 10;
    }
  }
  // back ende

  for (int i = 0; i <= 16; i++)
  {

    this->PixelOnOff[0][pin0Countdown] = 0; // backward
    this->PixelOnOff[0][pin0Countup] = 0;   // forward
    pin0Countup--;
    pin0Countdown++;

    this->PixelOnOff[1][pin1Countup] = 0;   // forward
    this->PixelOnOff[1][pin1Countdown] = 0; //backward
    pin1Countup--;
    pin1Countdown++;

    this->PixelOnOff[2][pin2Countup] = 0;   // forward
    this->PixelOnOff[2][pin2Countdown] = 0; //backward
    pin2Countup--;
    pin2Countdown++;

    this->AnimationHelper();
    delay(delaytime);
    delaytime += 10;
  }
  delay(1000);
}
/**
 * Helps to set the color of each LED
 */
void Led::AnimationHelper()
{
  // Serial.println(String("show it"));
  for (int i = 0; i < this->pinCount; i++)
  {
    this->setPin(this->pins[i]);
    for (int k = 0; k <= PIXEL_COUNT; k++)
    {
      if (this->PixelOnOff[i][k] == 1)
      {
        this->ShowColor(k, this->Red, this->Green, this->Blue); // rgb blue
      }
      else
      {
        this->ShowColor(k, 0, 0, 0); // rgb blue
      }
    }
    this->show();
  }
}

/** Rainbow Show
 * 
 */
void Led::StartRainbow()
{
  Serial.println("Rainbow");
  this->Rainbow(255, 0, 0);
  this->Rainbow(0, 255, 0);
  this->Rainbow(0, 0, 255);
  this->Rainbow(255, 255, 0); // yellow
  this->Rainbow(80, 0, 80);   // purple
  this->Rainbow(0, 255, 255); // aqua
}

/**
 * Start Show
 * @param byte ShowNumber
 */
void Led::StartStandardShow()
{
  Serial.println("Standard");

  for (int i = 0; i < this->pinCount; i++)
  {
    this->setPin(this->pins[i]);
    for (int k = 0; k <= PIXEL_COUNT; k++)
    {
      this->ShowColor(k, this->Red, this->Green, this->Blue); // rgb blue
    }
    this->show();
  }
}

/**
 * set color
 * @param int Red (0-255)
 * @param int Green (0-255)
 * @param int Blue (0-255)
 */
void Led::ShowColor(int pixelcnt, int Red, int Green, int Blue)
{
  this->setPixelColor(pixelcnt, this->Color(Red, Green, Blue));
}
/**
 * Rainbow Animation
 * @param int Red (0-255)
 * @param int Green (0-255)
 * @param int Blue (0-255) 
 */
void Led::Rainbow(int red, int green, int blue)
{

  while (this->Red != red || this->Green != green || this->Blue != blue)
  {
    if (this->Red < red)
      this->Red += 1;
    if (this->Red > red)
      this->Red -= 1;

    if (this->Green < green)
      this->Green += 1;
    if (this->Green > green)
      this->Green -= 1;

    if (this->Blue < blue)
      this->Blue += 1;
    if (this->Blue > blue)
      this->Blue -= 1;

    for (byte i = 0; i < this->pinCount; i++)
    {
      this->setPin(this->pins[i]);
      for (int k = 0; k <= PIXEL_COUNT; k++)
      {
        this->ShowColor(k, this->Red, this->Green, this->Blue); // rgb blue
      }
      this->show();
    }

    delay(10);
  }
}

// Init Leds
Led LedStripes;

// Replaces placeholder with Config Values state value
String processor(const String &var)
{
  if (var == "Brightness")
  {
    return String(wuerfel->brightness);
  }
  else if (var == "Mode1")
  {
    if (wuerfel->mode == 1)
    {
      return "selected";
    }
    else
    {
      return "";
    }
  }
  else if (var == "Mode2")
  {
    if (wuerfel->mode == 2)
    {
      return "selected";
    }
    else
    {
      return "";
    }
  }
  else if (var == "Mode3")
  {
    if (wuerfel->mode == 3)
    {
      return "selected";
    }
    else
    {
      return "";
    }
  }
  else if (var == "Mode4")
  {
    if (wuerfel->mode == 4)
    {
      return "selected";
    }
    else
    {
      return "";
    }
  }

  else if (var == "Red")
  {
    return String(wuerfel->r);
  }
  else if (var == "Green")
  {
    return String(wuerfel->g);
  }
  else if (var == "Blue")
  {
    return String(wuerfel->b);
  }
}

void setup()
{
  // set Leds off and ready
  Serial.begin(115200);
  delay(500); //delay for Monitor

  wifiManager.setTimeout(180);

  // read stored parameters
  EEPROM.begin(EEPROM_SIZE);
  eptr = (unsigned char *)wuerfel;
  for (int i = 0; i < sizeof(wuerfel_processor_struct); i++)
    *(eptr++) = EEPROM.read(i);

  // EEPROM-parameters invalid, use default-values and store them
  if (wuerfel->valid != 0xA5)
  {
    wuerfel->mode = 2;
    wuerfel->r = 100;
    wuerfel->g = 8;
    wuerfel->b = 0;
    wuerfel->brightness = 90;
    wuerfel->valid = 0xA5;

    eptr = (unsigned char *)wuerfel;
    for (int i = 0; i < sizeof(wuerfel_processor_struct); i++)
      EEPROM.write(i, *(eptr++));
    EEPROM.commit();
  }

  //fetches ssid and pass and tries to connect
  //if it does not connect it starts an access point with the specified name
  //here  "WuerfelAP"
  //and goes into a blocking loop awaiting configuration
  if (!wifiManager.autoConnect("WuerfelAP"))
  {
    Serial.println("failed to connect and hit timeout");
    delay(3000);
    //reset and try again, or maybe put it to deep sleep
    ESP.reset();
    delay(5000);
  }

  // Initialize SPIFFS
  if (!SPIFFS.begin())
  {
    Serial.println("An Error has occurred while mounting SPIFFS");
    return;
  }

  // Route for root / web page
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request) {
    request->send(SPIFFS, "/page.html", String(), false, processor);
  });

  // Route to load jscolor.js file
  server.on("/jscolor.js", HTTP_GET, [](AsyncWebServerRequest *request) {
    request->send(SPIFFS, "/jscolor.js", "text/plain");
  });
  // Route for Save Form Data
  server.on("/", HTTP_POST, [](AsyncWebServerRequest *request) {
    int params = request->params();
    for (int i = 0; i < params; i++)
    {
      AsyncWebParameter *p = request->getParam(i);
      if (p->name() == "MODE")
      {
        wuerfel->mode = strtol(p->value().c_str(), NULL, 10);
      }
      if (p->name() == "BRIGHT")
      {
        wuerfel->brightness = strtol(p->value().c_str(), NULL, 10);
      }
      if (p->name() == "RED")
      {
        wuerfel->r = strtol(p->value().c_str(), NULL, 10);
      }
      if (p->name() == "GREEN")
      {
        wuerfel->g = strtol(p->value().c_str(), NULL, 10);
      }
      if (p->name() == "BLUE")
      {
        wuerfel->b = strtol(p->value().c_str(), NULL, 10);
      }
    }

    unsigned int j;
    unsigned char *eptr = (unsigned char *)wuerfel;

    for (j = 0; j < sizeof(wuerfel_processor_struct); j++)
      EEPROM.write(j, *(eptr++));
    EEPROM.commit();

    request->send(SPIFFS, "/page.html", String(), false, processor);
  });

  // start web-server
  server.begin();
  Serial.println("Web server started!");

  // if you get here you have connected to the WiFi
  Serial.println("connected...yeey :)");

  LedStripes.setup(PinArray, (sizeof(PinArray) / sizeof(PinArray[0])), PIXEL_COUNT);
  Serial.println("setup...yeey :)");
  LedStripes.setPixelColors(wuerfel->r, wuerfel->g, wuerfel->b);
  Serial.println("color...yeey :)");
  // setBrightness
  LedStripes.setBrightness(wuerfel->brightness);

  LedStripes.Start();
}

void loop()
{
  Serial.println("loop...yeey :)");
  LedStripes.setPixelColors(wuerfel->r, wuerfel->g, wuerfel->b);
  LedStripes.setBrightness(wuerfel->brightness);
  LedStripes.SelectShow(wuerfel->mode);
  Serial.println(wuerfel->mode);
}
